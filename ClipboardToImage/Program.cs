﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ClipboardToImage
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Image cb = Clipboard.GetImage();
            if (cb != null)
            {
                cb.Save("clipboard.png");
            }

            Application.Exit();
        }
    }
}
